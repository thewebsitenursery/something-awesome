# ***Something Awesome***

## Coming Soon template for new web projects
### Automaticly redirects to a video or image slider background depending on users device / browser

Example at: https://thewebsitenursery.com/awesome

__fork for new projects and following make changes:__

* The title and meta tags in index.html to match the new project
* The logo / favicons / app icons (favicon-generator.org for favicon and app icons)
* optional: bg-video / bg-images changed to align with new project
* check css and html for bugs etc.

__git clone into the root directory of new project webserver root:__

i.e. cd /var/www/html && git clone https://bitbucket.org/thewebsitenursery/something-awesome .

__Secure Project using Lets Encrypt__